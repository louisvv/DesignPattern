package FactoryMethod;

/**
 * Created by louisvv.
 * Date:2018/6/12
 * Time:14:30
 * Desc:创建香蕉工厂
 */
public class BananaFactory implements FruitFactory {
    public Fruit getFruit() {
        return new Banana();
    }
}
