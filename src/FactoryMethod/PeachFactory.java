package FactoryMethod;

/**
 * Created by louisvv.
 * Date:2018/6/12
 * Time:14:44
 * Desc:
 */
public class PeachFactory implements FruitFactory {
    public Fruit getFruit() {
        return new Peach();
    }
}
