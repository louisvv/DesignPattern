package FactoryMethod;


/**
 * Created by yanwei on 2018/6/12.
 */

//  定义苹果工厂类
public class AppleFactory implements FruitFactory{
    public Fruit getFruit() {
        return new Apple();
    }
}
