package FactoryMethod;

/**
 * Created by yanwei on 2018/6/12.
 */
public class User {
    public static void main(String[] args) {
        //  创建苹果工厂
        FruitFactory appleFactory = new AppleFactory();
        //  通过苹果工厂创建苹果实例对象
        Fruit apple = appleFactory.getFruit();
        apple.show();

        //  创建香蕉工厂
        FruitFactory bananaFactory = new BananaFactory();
        //  通过香蕉工厂创建香蕉实例对象
        Fruit banana = bananaFactory.getFruit();
        banana.show();

        //  创建桃子工厂
        FruitFactory peachFactory = new PeachFactory();
        //  通过桃子工厂创建桃子实例对象
        Fruit peach = new Peach();
        peach.show();
    }
}
