package FactoryMethod;

/**
 * Created by yanwei on 2018/5/29.
 */

//  创建水果工厂
public interface FruitFactory {
    public Fruit getFruit();
}
