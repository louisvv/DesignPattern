package FactoryMethod;

/**
 * Created by louisvv.
 * Date:2018/6/12
 * Time:14:42
 * Desc:定义桃子类
 */
public class Peach extends Fruit{
    public void show() {
        System.out.println("我是桃子");
    }
}
