package Singleton;

/**
 * Created by louisvv.
 * Date:2018/6/14
 * Time:10:58
 * Desc:测试类
 */
public class SingletonTest {
    public static class Mythread extends Thread {
        @Override
        public void run() {
            super.run();
            System.out.println(SingletonStatic.getInstance());
        }
    }
    public static void main(String[] args) {
        Mythread mythread1 = new Mythread();
        Mythread mythread2 = new Mythread();
        Mythread mythread3 = new Mythread();
        mythread1.start();
        mythread2.start();
        mythread3.start();
    }
}
