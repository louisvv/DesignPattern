package Singleton;

/**
 * Created by louisvv.
 * Date:2018/6/14
 * Time:13:49
 * Desc:单例模式懒汉式（线程安全）
 */
public class LazySingletonSafe {
    //  初始化一个对象，对象为null
    private static LazySingletonSafe lazySingletonSafe;
    //  将构造函数私有
    private LazySingletonSafe() {}

    //  创建外部访问函数，添加synchronized锁，保证线程安全
    public static synchronized LazySingletonSafe getInstance() {
        //  判断对象是否为null
        if (lazySingletonSafe == null) {
            //  如果不为空，则创建一个LazySingleton对象
            lazySingletonSafe =new LazySingletonSafe();
            return lazySingletonSafe;
        }
        return lazySingletonSafe;
    }
}
