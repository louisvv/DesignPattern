package Singleton;

/**
 * Created by louisvv.
 * Date:2018/6/14
 * Time:14:48
 * Desc:
 */
public class SingletonStatic {
    //  私有构造方法
    private SingletonStatic() {};

    //  编写静态内部类
    private static class SingletonStaticHandle {
        //  内部类中，创建静态对象
        private static final SingletonStatic singletonStatic = new SingletonStatic();
    }

    //  编写外部访问方法
    public static SingletonStatic getInstance() {
        return SingletonStaticHandle.singletonStatic;
    }

}
