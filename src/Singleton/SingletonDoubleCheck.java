package Singleton;

/**
 * Created by louisvv.
 * Date:2018/6/14
 * Time:14:19
 * Desc:单例模式双重校验锁
 */
public class SingletonDoubleCheck {
    //  初始化singletonDoubleCheck对象，该对象为null
    private static SingletonDoubleCheck singletonDoubleCheck;

    //  将构造函数私有
    private SingletonDoubleCheck() {}

    //  创建外部访问函数
    public static SingletonDoubleCheck getInstance() {
        //  首先对singletonDoubleCheck进行判断，是否为空
        if (singletonDoubleCheck == null) {
            //  如果为空，使用同步锁对创建SingletonDoubleCheck过程进行修饰
            synchronized (SingletonDoubleCheck.class) {
                //  再次进行判断，singletonDoubleCheck是否为空
                if (singletonDoubleCheck == null) {
                    singletonDoubleCheck = new SingletonDoubleCheck();
                }
                return singletonDoubleCheck;
            }
        }
        return singletonDoubleCheck;
    }


}
