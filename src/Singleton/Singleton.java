package Singleton;

/**
 * Created by louisvv.
 * Date:2018/6/14
 * Time:10:52
 * Desc:饿汉式单例模式
 */
public class Singleton {
    //  创建对象
    private static Singleton singleton = new Singleton();

    //  将构造函数私有
    private Singleton() {}

    //  创建外部访问方法
    public static Singleton getInstance() {
        return singleton;
    }
}
