package Singleton;

/**
 * Created by louisvv.
 * Date:2018/6/14
 * Time:11:12
 * Desc:懒汉式单例模式
 */
public class LazySingleton {
    //  初始化一个对象，对象为null
    private static LazySingleton lazySingleton;

    //  将构造函数私有
    private LazySingleton() {}

    //  创建外部访问函数
    public static LazySingleton getInstance() {
        //  判断对象是否为null
        if (lazySingleton == null) {
            //  如果不为空，则创建一个LazySingleton对象
            return new LazySingleton();
        }
        return lazySingleton;
    }
}
