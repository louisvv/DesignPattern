package AbstractFactory;

/**
 * Created by louisvv.
 * Date:2018/6/13
 * Time:14:42
 * Desc:创建苹果手机具体产品Iphone X
 */
public class IphoneX extends ApplePhone {
    public void show() {
        System.out.println("Iphone X");
    }
}
