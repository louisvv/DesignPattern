package AbstractFactory;

/**
 * Created by louisvv.
 * Date:2018/6/13
 * Time:14:58
 * Desc:
 */
public class SamsungFactory implements AbstractFactory {
    public Phone getPhone(String phoneType) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        //Class phone=Class.forName(phoneType);
        return (SamsungPhone)Class.forName(phoneType).newInstance();
    }
}
