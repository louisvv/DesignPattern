package AbstractFactory;

/**
 * Created by louisvv.
 * Date:2018/6/13
 * Time:14:48
 * Desc:创建三星手机具体产品s9
 */
public class GalaxyS9 extends SamsungPhone {
    public void show() {
        System.out.println("Galaxy S9");
    }
}
