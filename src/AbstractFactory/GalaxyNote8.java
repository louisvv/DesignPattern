package AbstractFactory;

/**
 * Created by louisvv.
 * Date:2018/6/13
 * Time:14:47
 * Desc:创建三星手机具体产品note 8
 */
public class GalaxyNote8 extends SamsungPhone {
    public  void show() {
        System.out.println("Galaxy Note 8");
    }
}
