package AbstractFactory;

/**
 * Created by louisvv.
 * Date:2018/6/13
 * Time:14:52
 * Desc:创建iphone手机工厂
 */
public class AppleFactory implements AbstractFactory {
    public Phone getPhone(String phoneType) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class iphone=Class.forName(phoneType);
        return (ApplePhone) iphone.newInstance();
    }
}
