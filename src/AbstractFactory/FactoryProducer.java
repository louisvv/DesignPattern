package AbstractFactory;

/**
 * Created by louisvv.
 * Date:2018/6/13
 * Time:15:55
 * Desc:超级工厂类，用于创建工厂
 */
public class FactoryProducer {
    public static AbstractFactory getFactory(String factoryName) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class factory=Class.forName(factoryName);
        return (AppleFactory)factory.newInstance();
    }
}
