package AbstractFactory;

/**
 * Created by louisvv.
 * Date:2018/6/13
 * Time:14:24
 * Desc:最高级抽象，产品族
 */
public interface Phone {
   public void show();
}
