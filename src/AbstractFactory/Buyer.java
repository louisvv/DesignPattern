package AbstractFactory;

/**
 * Created by louisvv.
 * Date:2018/6/13
 * Time:15:00
 * Desc:用户类
 */
public class Buyer {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException, ClassNotFoundException {

        //  先问客户要什么牌子的手机，客户说要苹果的
        //  创建苹果手机工厂
        AbstractFactory appleFcatory = FactoryProducer.getFactory("AbstractFactory.AppleFactory");
        //  来到专卖店，问客户要什么类型的手机，客户说要iphone X
        System.out.println("客户想要买苹果iphone X手机");
        Phone iphoneX = appleFcatory.getPhone("AbstractFactory.IphoneX");
        iphoneX.show();

        System.out.println("客户想要买苹果iphone 8手机");
        Phone iphone8=appleFcatory.getPhone("AbstractFactory.Iphone8");
        iphone8.show();

        //  先问客户要什么牌子的手机，客户说要三星的
        //  创建三星手机工厂
        AbstractFactory samsungFcatory = FactoryProducer.getFactory("AbstractFactory.SamsungFactory");
        System.out.println("客户想要买三星s9手机");
        Phone s9=samsungFcatory.getPhone("AbstractFactory.GalaxyS9");
        s9.show();

        System.out.println("客户想要买三星note 8手机");
        Phone note8=samsungFcatory.getPhone("AbstractFactory.GalaxyNote8");
        note8.show();
    }
}
