package AbstractFactory;

/**
 * Created by louisvv.
 * Date:2018/6/13
 * Time:14:43
 * Desc:创建苹果手机具体产品Iphone 8
 */
public class Iphone8 extends ApplePhone {
    public  void show() {
        System.out.println("Iphone 8");
    }
}
