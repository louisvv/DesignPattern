package AbstractFactory;

/**
 * Created by louisvv.
 * Date:2018/6/13
 * Time:14:50
 * Desc:抽象工厂
 */
public interface AbstractFactory {
    public Phone getPhone(String phoneType) throws ClassNotFoundException, IllegalAccessException, InstantiationException;
}
