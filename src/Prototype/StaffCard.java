package Prototype;

/**
 * Created by louisvv.
 * Date:2018/6/20
 * Time:14:01
 * Desc:浅克隆
 */
public class StaffCard implements Cloneable{
    //  创建company类
    Company company=new Company();
    //  部门
    private String department;
    //  姓名
    private String name;
    //  职位
    private String job;
    //  员工编号
    private int id;
    //  显示员工信息
    public void show(){
        System.out.println(name + ",编号:"+id+",在"+getCompany().getName()+department+"担任:"+job);
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    //  实现clone方法
    public StaffCard clone(){
        try {
            return (StaffCard) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

}
