package Prototype;

/**
 * Created by louisvv.
 * Date:2018/6/20
 * Time:17:03
 * Desc:公司类
 */
public class Company implements Cloneable{
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Company clone(){
        try {
            return (Company) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
}
