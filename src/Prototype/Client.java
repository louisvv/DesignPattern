package Prototype;

/**
 * Created by louisvv.
 * Date:2018/6/20
 * Time:14:58
 * Desc:客户端
 */
public class Client {
    public static void main(String[] args) {
       /* StaffCard staffCard=new StaffCard();
        staffCard.setId(100);
        staffCard.setDepartment("开发部");
        staffCard.setJob("java开发工程师");
        staffCard.company.setName("louisvv公司");
        staffCard.setName("Tom");

        StaffCard staffCard2=staffCard.clone();
        staffCard2.setName("Jerry");
        staffCard2.setJob("前端开发工程师");
        staffCard2.setId(101);
        //  打印员工信息
        staffCard.show();
        staffCard2.show();
        System.out.println("--------------");
        //  打印对象哈希码，来判断是否是同一个对象
        System.out.println(staffCard);
        System.out.println(staffCard2);
        System.out.println("--------------");
        //  打印company对象的哈希码
        System.out.println(staffCard.getCompany().hashCode());
        System.out.println(staffCard2.getCompany().hashCode());

        staffCard.company.setName("balabala公司");
        staffCard.show();
        staffCard2.show();*/

        DeepStaffCard deepStaffCard = new DeepStaffCard();
        deepStaffCard.company.setName("louisvv公司");
        deepStaffCard.setDepartment("开发部");
        deepStaffCard.setName("Tom");
        deepStaffCard.setJob("java开发工程师");
        deepStaffCard.setId(102);

        DeepStaffCard deepStaffCard2 = deepStaffCard.clone();
        deepStaffCard2.setId(103);
        deepStaffCard2.setName("Jerry");
        deepStaffCard2.setJob("前端开发工程师");

       /* deepStaffCard.show();
        deepStaffCard2.show();
        System.out.println("--------------");
        System.out.println(deepStaffCard.getCompany().hashCode());
        System.out.println(deepStaffCard2.getCompany().hashCode());*/

        deepStaffCard.company.setName("balabala公司");
        deepStaffCard.show();
        deepStaffCard2.show();

    }
}
