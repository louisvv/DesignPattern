package SimpleFactory;

/**
 * Created by yanwei on 2018/5/29.
 */

//  创建用户类，外部访问工厂静态方法
public class User {
    public static void main(String[] args) throws Exception {
        try {
            Fruit apple = FruitFactory.getFruit("apple");
            apple.show();
        } catch (NullPointerException e) {
            System.out.println("没有这类水果");
        }

        try {
            Fruit banana = FruitFactory.getFruit("banana");
            banana.show();
        } catch (NullPointerException e) {
            System.out.println("没有这类水果");
        }

        try {
            Fruit peach = FruitFactory.getFruit("peach");
            peach.show();
        } catch (NullPointerException e) {
            System.out.println("没有这类水果");
        }

    }
}
