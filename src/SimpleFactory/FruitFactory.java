package SimpleFactory;

/**
 * Created by yanwei on 2018/5/29.
 */

//  创建水果工厂
public class FruitFactory {
    public static Fruit getFruit(String fruitType) throws IllegalAccessException, InstantiationException {
        switch (fruitType.toLowerCase()){
            case "apple":
                return new Apple();
            case "banana":
                return new Banana();
            default:
                return null;
        }
    }
}
