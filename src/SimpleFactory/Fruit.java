package SimpleFactory;

/**
 * Created by yanwei on 2018/5/30.
 */

//  创建水果抽象类
public abstract class Fruit {
    public abstract void show();
}
